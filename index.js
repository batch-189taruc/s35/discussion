

const express = require('express')

const dotenv = require('dotenv')

const mongoose = require('mongoose')

const app = express()

const port = 3001

dotenv.config()

// MONGOOSE CONNECTION
// Mongoose uses the 'connect' function to connect to the cluster in our MongoDB Atlas.
/*
	It takes 2 arguments:
		1. Connection string from MongoDB Atlas
		2. Object that contains the middleware/standard that MongoDB uses


*/
mongoose.connect(`mongodb+srv://zuittdiscussion:${process.env.MONGODB_PASSWORD}@zuitt-bootcamp.xbtkvqt.mongodb.net/?retryWrites=true&w=majority`, {
	useNewUrlParser : true,
	useUnifiedTopology : true
	}
)

// initialize the mongoose connection to the MongoDB database by assigning 'mongoose.connection' to the 'db' variable
let db = mongoose.connection

// Listen to the event of the connection by using the '.on()' of the mongoose.connection. Then, log details in the console based on the event (error or success - open)
db.on('error', console.error.bind(console, "Connection Error"))
db.on('open', () => console.log('Connected to MongoDB!'))

//Creating a Schema
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: 'Pending'
	}
})

// CREATING A MODEL
//model naming convention should be first letter capital such as "Task"

const Task = mongoose.model('Task', taskSchema)


// CREATING THE ROUTES

//our JSON middleware for express.js
app.use(express.json()) 
app.use(express.urlencoded({extended: true}))

// Create single task route
app.post('/tasks',(req,res) => {
	//Use the Task model to find a similar entry in the database based on the user input from the request.body (or from postman)
	Task.findOne({name: req.body.name}, (error, result) =>{
		//check if the name from postman body has a similar entry in the database
		if(result != null && result.name == req.body.name){
			return res.send('Duplicate task found!')
		} else{
			//if there are no duplicate entries, then create a new task out of the data/name that was placed
			let newTask = new Task({
				name: req.body.name
			})

			newTask.save((error, savedTask) => {
				if(error){
					return console.error(error)
				}
				//If there no errors upon saving, return a response with a status of 201
				return res.status(201).send('New Task Created!')
			})
		}
	})

})

// View collection or Get all taks
app.get('/tasks', (req,res) => {
	Task.find({}, (error, result) => {
		if(error){
			return res.send(error)
		}
		return res.send(result)
	})
})

// USERS COLLECTION
//Schema
const userSchema = new mongoose.Schema({
	username : String,
	password: String
})
//Model

const User = mongoose.model('User', userSchema)

//Routes
// register a user
app.post('/register', (req,res) => {
	User.findOne({username: req.body.username}, (error, result) => {
	//check for duplicates
	if(result != null && result.username == req.body.username){
		return res.send('Duplicate user found!')
		//if no duplicates
	} else{
		if(req.body.username !== '' && req.body.password !==''){
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			})

			newUser.save((error,savedUser) =>{
				if(error){
					return res.send(error)
				}
				return res.send('New user has been registered!')
				/*or use
				return res.send({
					message: 'New User has been registered!',
					data : savedUser
				})


				 */
			})
		} else{
			return res.send('BOTH Username AND Password must be provided')
		}
	}
})
})


// view the registered users


app.get('/users', (req, res) => {
	return User.find({}, (error, result) => {
		if(error){
			res.send(error)
		}
		res.send(result)
	})
})




// app.listen should ALWAYS be at the bottom of the server
app.listen(port, () => console.log(`Server is running at port ${port}`))